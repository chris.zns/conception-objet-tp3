from tp2 import *

def test_box_create():
	b = Box()

def test_box_add():
	b= Box()
	b.add("truc1")
	b.add("truc2")

#~ On veut pouvoir tester si un truc est dans une boite
def test_box_contains():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	assert "truc1" in b
	assert "truc2" in b
	assert "truc3" not in b

def test_box_enlever():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	b.enlever("truc2")
	assert "truc1" in b
	assert "truc2" not in b
	
def test_ouvrir():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	b.open()
	assert b.is_open()
	
def test_fermer():
	b = Box()
	b.add("truc1")
	b.add("truc2")
	b.close()
	assert not b.is_open()
	
def test_lookouvert():
	_ouvert=True
	b = Box()
	b.add("truc1")
	b.add("truc2")
	b.action_look()
	
def test_lookferme():
	_ouvert=False
	b= Box()
	b.action_look()
	
def test_thing_create():
	t=Thing(3)
	
def test_thing():
	t=Thing(3)
	assert t.volume() == 3

def test_capacite():
	b=Box()
	b.set_capacity(5)
	assert b.capacity() == 5
	
def test_capacitenone():
	b=Box()
	assert b.capacity() == None
	
	
	
