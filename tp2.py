class Box:
	def __init__(self):
		self._contents = []
		self._ouvert=True
		self._capacity=None
		
	def add(self, truc):
		self._contents.append(truc)

	def __contains__(self, machin):
		return machin in self._contents

	def enlever(self, truc):
	
		self._contents.remove(truc)
	
	def open(self):
		self._ouvert=True
	
	def close(self):
		self._ouvert=False
	
	def is_open(self):
		return self._ouvert
		
	def action_look(self):
		res="la boite est fermee"
		tmp=self._contents
		if self.is_open():
			res="la boite contient :"
			res+=", ".join(tmp)
		return res
		
	def set_capacity(self, capacite):
		self._capacity=capacite
		
	def capacity(self):
		return self._capacity
				
class Thing:
	
	def __init__(self,volume):
		self._volume=volume
		
	def volume(self):
		return self._volume
			
